# README #

> A conspicuously outdated vision of the future. [TVTropes](http://tvtropes.org/pmwiki/pmwiki.php/Main/Zeerust) 

Zeerust: making retro games easily with modern technology.

It aims to be a complete solution for oldchool pixel-perfect 2D games which combines high-levelness of game constructors with raw power of general purpose programming language. It offers (or is going to do so):

* Minimal and simplistic yet flexible and extendable engine written in Rust
* Cross-platform support (made possible with [ggez](https://github.com/ggez/ggez))
* Game objects modularized with entity-component-system approach (based on [Specs](https://github.com/slide-rs/specs))
* Built-in systems for handling typical game tasks (movement, simple collision checking, keyboard events)
* *(not yet)* eDSL for describing game entities without boilerplate 
* *(not yet)* Support for indexed colors, palette swaps and chiptune formats out-of-the-box
* *(not yet)* Level editor with tile support

Zeerust is on the very early stages of development, so the architecture and API may (and will) drastically change in future.

## Examples ##
(some 50-lines Pong or Snake should be added here)

For other demos, look at *examples* source directory. They can be run with 
```
$ cargo run --example _examplename_
```
