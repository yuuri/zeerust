#![allow(dead_code)]

use graphics::*;
use common::*;
use std::collections::HashMap;
use std::sync::Arc;

/// Cell of rectangular grid from (0, 0) to (65535, 65535).
pub type Cell = (u16, u16);

/// A collection of indexed tiles with their data.
/// For example, for each tile you can store if it is solid, sloped (or a collision mask),
/// deals damage (spikes/lava) - anything you want.
pub trait Tileset {

    /// A type to index the tile.
    type Id: Copy;

    /// A type to hold associated data for tiles.
    type Data;

    /// Gets a reference to the tile associated data.
    fn get(&self, i: Self::Id) -> &Self::Data;

    /// Gets a mutable reference to the tile data. Used for tile editors.
    fn get_mut(&mut self, i: Self::Id) -> &mut Self::Data;

    /// Returns a tile size (in floats).
    fn tile_size(&self) -> Size;

    /// Draws a tile on the `Context` position.
    fn draw_tile(&self, ctx: &mut Context, dest: Point, i: Self::Id);
}

/// A collection of tiles on the screen.
pub struct TileLayer<TS> where TS: Tileset {
    tileset: Arc<TS>,
    tiles: HashMap<Cell, TS::Id>
}

impl<TS> TileLayer<TS> where TS: Tileset {
    pub fn new(ts: Arc<TS>) -> TileLayer<TS> {
        TileLayer::<TS> {
            tileset: ts,
            tiles: HashMap::<Cell, TS::Id>::new()
        }
    }

    /// Returns an optional reference to the tile data in the cell of a screen grid, if there is one.
    pub fn tile_data(&self, pos: Cell) -> Option<&TS::Data> {
        self.tiles.get(&pos).map(|&id| self.tileset.get(id)) //TODO: why &pos?
    }

    /// Returns an optional reference to the tile data by the room co-ordinates, if there is one.
    pub fn tile_data_pos(&self, pos: Point) -> Option<&TS::Data> {
        let size = self.tileset.tile_size();
        let cell = ((pos.x / size.x).floor() as u16,
                    (pos.y / size.y).floor() as u16);
        self.tile_data(cell)
    }

    /// Adds a tile to the grid.
    pub fn add(&mut self, pos: Cell, id: TS::Id) {
        self.tiles.insert(pos, id);
    }

    /// Removes a tile from the grid.
    pub fn remove(&mut self, pos: Cell) {
        self.tiles.remove(&pos);
    }
}

impl<TS> Drawable for TileLayer<TS> where TS: Tileset {
    fn draw_ex(&self, ctx: &mut Context, param: DrawParam) -> GameResult<()> {
        let ts = self.tileset.as_ref();
        let size = self.tileset.tile_size();
        for (&(x, y), id) in &self.tiles {
            let dest = param.dest + Vector::new(x as f32 * size.x, y as f32 * size.y);
            ts.draw_tile(ctx, dest, *id)
        }
        Ok(())
    }

    fn set_blend_mode(&mut self, mode: Option<BlendMode>) {
    }

    fn get_blend_mode(&self) -> Option<BlendMode> {
        None
    }
}

//Some implementations

/// Generic tileset with WxH tiles indexed by integer positions in the grid.
pub struct GenericTileset<TD> {
    image: Image,
    tile_size: Size,
    xsize: f32, //precomputed tile size as the fraction of the whole image (0.0 - 1.0)
    ysize: f32, //
    default_data: TD,
    tiles_data: HashMap<Cell, TD>,
}

impl<TD> GenericTileset<TD> where TD: Default {
    pub fn new(image: Image, tile_size: Size) -> GenericTileset<TD> {
        let w = image.width() as f32;
        let h = image.height() as f32;
        let xsize = tile_size.x / w;
        let ysize = tile_size.y / h;
        GenericTileset::<TD> {
            image, tile_size, xsize, ysize,
            default_data: TD::default(),
            tiles_data: HashMap::<Cell, TD>::new()
        }
    }
}

impl<TD> Tileset for GenericTileset<TD> where TD: Default {
    type Id = Cell;
    type Data = TD;

    fn tile_size(&self) -> Size {
        self.tile_size
    }

    fn get(&self, i: Self::Id) -> &TD {
        match self.tiles_data.get(&i) {
            Some(td) => td,
            None => &self.default_data
        }
    }

    fn get_mut(&mut self, i: Self::Id) -> &mut TD {
        self.tiles_data.entry(i).or_insert(TD::default())
    }

    fn draw_tile(&self, ctx: &mut Context, dest: Point, i: Self::Id) {
        let (x, y) = i;
        let rect = Rect {
            x: x as f32 * self.xsize,
            y: y as f32 * self.ysize,
            w: self.xsize,
            h: self.ysize
        };
        let mut param = DrawParam::default();
        param.src = rect;
        param.dest = dest;
        let _ = self.image.draw_ex(ctx, param);
    }
}

//Blocky tile of 2x2 sub-tiles

type Block = [u8; 4];

struct Block8BitTileset<TD> {
    image: Image,
    tiles: [(Block, TD); 256]
}

impl<TD> Block8BitTileset<TD> where TD: Default + Copy {
    fn new(image: Image) -> Block8BitTileset<TD> {
        Block8BitTileset::<TD> {
            image: image,
            tiles: [(Block::default(), TD::default()); 256]
        }
    }
}

impl<TD> Tileset for Block8BitTileset<TD> where TD: Default {
    type Id = u8;
    type Data = TD;

    fn tile_size(&self) -> Size {
        Size {x: 16., y: 16.}
    }

    fn get(&self, i: Self::Id) -> &TD {
        let (_, ref td) = self.tiles[i as usize];
        td
    }

    fn get_mut(&mut self, i: Self::Id) -> &mut TD {
        let (_, ref mut td) = self.tiles[i as usize];
        td
    }

    fn draw_tile(&self, ctx: &mut Context, dest: Point, i: Self::Id) {
        const SCALE: f32 = 8. / 256.;
        let mut param = DrawParam::default();
        let mut draw_sub = |id: u8, xoff: u8, yoff: u8| {
            let (x, y) = (id & 0x0F, i & 0xF0);
            let rect = Rect {
                x: x as f32 * SCALE,
                y: y as f32 * SCALE,
                w: SCALE,
                h: SCALE
            };

            param.src = rect;
            param.dest.x = dest.x + xoff as f32;
            param.dest.y = dest.y + yoff as f32;
            let _ = self.image.draw_ex(ctx, param); //TODO: handle errors
        };
        let &(subs, _) = &self.tiles[i as usize];
        draw_sub(subs[0], 0, 0);
        draw_sub(subs[1], 8, 0);
        draw_sub(subs[2], 0, 8);
        draw_sub(subs[3], 8, 8);
    }
}
