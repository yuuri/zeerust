use common::Point;
pub use ggez::{Context, GameResult};
pub use ggez::graphics::{BlendMode, Rect, Image, Drawable, DrawParam};
use specs::{Component, System, ReadStorage, Join};
use std::marker::PhantomData;
use movement::{Pos};

pub struct DrawSys<'a, T> {
    ctx: &'a mut Context,
    phantom: PhantomData<T>
}

impl<'a, T> DrawSys<'a, T> {
    pub fn new(ctx: &'a mut Context) -> DrawSys<'a, T> {
        DrawSys {ctx: ctx, phantom: PhantomData}
    }
}

impl<'a, 'b, T: Drawable + Component> System<'a> for DrawSys<'b, T> {
    type SystemData = (ReadStorage<'a, Pos>, ReadStorage<'a, T>);

    fn run(&mut self, (poss, targets): Self::SystemData) {
        for (pos, target) in (&poss, &targets).join() {
            let _ = target.draw(self.ctx, Point::new(pos.x, pos.y), 0.0); //TODO: handle errors
        }
    }
}
