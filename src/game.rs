use movement::{MoveSys, AccSys};
use sprite::Sprite;
use graphics::{DrawSys};
use background::*;
use control::*;
use room::{Room};
use specs::RunNow;
use specs::{Dispatcher, DispatcherBuilder};
use ggez::{Context, GameResult, graphics, event};

pub struct Game<'a, 'b> {
    room: Room,
    on_step: Dispatcher<'a, 'b>,
    on_key_down: Dispatcher<'a, 'b>,
    on_key_up: Dispatcher<'a, 'b>
}

pub struct GameBuilder<'a, 'b> {
    room: Room,
    pub on_step: DispatcherBuilder<'a, 'b>,
    pub on_key_down: DispatcherBuilder<'a, 'b>,
    pub on_key_up: DispatcherBuilder<'a, 'b>
}

impl<'a, 'b> GameBuilder<'a, 'b> {
    pub fn new(mut room: Room) -> GameBuilder<'a, 'b> {
        room.world.add_resource(KbdState{keycode: Keycode::Return});
        GameBuilder {
            room: room,
            on_step: DispatcherBuilder::new()
                .with(MoveSys, "move", &[])
                .with(AccSys, "acc", &["move"]),
            on_key_down: DispatcherBuilder::new(),
            on_key_up: DispatcherBuilder::new()
        }
    }
    pub fn build(self) -> Game<'a, 'b> {
        Game {
            room: self.room,
            on_step: self.on_step.build(),
            on_key_down: self.on_key_down.build(),
            on_key_up: self.on_key_up.build(),
        }
    }
}

impl<'a, 'b> event::EventHandler for Game<'a, 'b> {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        self.on_step.dispatch(&mut self.room.world.res);
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);
        {
            let mut back_sys = DrawSys::<Background>::new(ctx);
            back_sys.run_now(&self.room.world.res);
        }
        {
            let mut draw_sys = DrawSys::<Sprite>::new(ctx);
            draw_sys.run_now(&self.room.world.res);
        }
        graphics::present(ctx);
        Ok(())
    }

    fn key_down_event(&mut self, _ctx: &mut Context, keycode: Keycode,
                      _keymod: event::Mod, _repeat: bool) {
        {
            let mut kbd = self.room.world.write_resource::<KbdState>();
            *kbd = KbdState{keycode: keycode};
        }
        self.on_key_down.dispatch(&mut self.room.world.res);
    }

    fn key_up_event(&mut self, _ctx: &mut Context, keycode: Keycode,
                    _keymod: event::Mod, _repeat: bool) {
        {
            let mut kbd = self.room.world.write_resource::<KbdState>();
            *kbd = KbdState{keycode: keycode};
        }
        self.on_key_up.dispatch(&mut self.room.world.res);
    }
}
