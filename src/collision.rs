use specs::{Component, DenseVecStorage};
//use specs::{Entity, Entities, System, ReadStorage, Fetch, Join}
pub use ggez::graphics::Rect;
pub use common::Size;
use movement::Pos;

#[derive(Clone)]
pub struct BBox {
    pub rect: Rect,
    pub solid: bool
}

/*
impl BBox {
    pub fn new(x: f32, y: f32, solid: bool) -> BBox {
        BBox{size: Size {x: x, y: y}, solid: solid}
    }
}
*/

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Direction {
    Right, Up, Left, Down
}

//CONCERN: epsilon?
pub fn rect_collide(r1: &Rect, r2: &Rect) -> Option<Direction> {
    let dl = r1.x + r1.w - r2.x;
    let dr = r2.x + r2.w - r1.x;
    let du = r1.y + r1.h - r2.y;
    let dd = r2.y + r2.h - r1.y;

    //TODO: consider epsilon
    if dl >= 0. && dr >= 0. && du >= 0. && dd >= 0. {
        let dx = if dl > dr {dr} else {dl};
        let dy = if du > dd {dd} else {du};
        if dy < dx {
            if du > dd {
                Some(Direction::Down)
            } else {
                Some(Direction::Up)
            }
        } else {
            if dr > dl {
                Some(Direction::Left)
            } else {
                Some(Direction::Right)
            }
        }
    } else {
        None
    }
}

fn rel(p: &Pos, b: &Rect) -> Rect {
    Rect {x: b.x + p.x, y: b.y + p.y, w: b.w, h: b.h}
}

pub fn bbox_collide(p1: &Pos, b1: &BBox, p2: &Pos, b2: &BBox) -> Option<Direction> {
    rect_collide(&rel(p1, &b1.rect), &rel(p2, &b2.rect))
}

#[test]
fn should_collide() {
    //Up
    assert_eq!(rect_collide(&Rect::new(0., 0., 2., 2.), &Rect::new(0., 1., 2., 2.)), Some(Direction::Up));
    //Down
    assert_eq!(rect_collide(&Rect::new(0., 1., 2., 2.), &Rect::new(0., 0., 2., 2.)), Some(Direction::Down));
    //Left
    assert_eq!(rect_collide(&Rect::new(0., 0., 2., 2.), &Rect::new(1., 0., 2., 2.)), Some(Direction::Left));
    //Right
    assert_eq!(rect_collide(&Rect::new(1., 0., 2., 2.), &Rect::new(0., 0., 2., 2.)), Some(Direction::Right));
}

#[test]
fn shouldnt_collide() {
    let r1 = Rect::new(0., 0., 2.1, 1.);
    let r2 = Rect::new(2., 2., 2., 2.);
    assert!(rect_collide(&r1, &r2).is_none());
}

impl Component for BBox {
    type Storage = DenseVecStorage<Self>;
}

/*
pub struct CollisionInfo(pub BType, pub Entity, pub BType, pub Entity);

pub struct ColliderSys {
    collisions: Vec<CollisionInfo>,
    handlers: BTreeMap<(BType, BType), Box<CollisionHandler>>
}

impl ColliderSys {
    pub fn new() -> ColliderSys {
        ColliderSys {
            collisions: Vec::new(),
            handlers: BTreeMap::new()
        }
    }

    pub fn add_handler(&mut self, t1: BType, t2: BType, func: Box<CollisionHandler>) {
        self.handlers.insert((t1, t2), func);
    }

    pub fn process(&mut self, world: &mut World) {
        for info in &self.collisions {
            let &CollisionInfo(t1, e1, t2, e2) = info;
            match self.handlers.get(&(t1, t2)) {
                Some(handler) => handler(world, e1, e2),
                None => panic!("Unknown collision between {} and {}", t1, t2)
            }
        }
        self.collisions.clear();
    }
}

impl<'a> System<'a> for ColliderSys {
    type SystemData = (Entities<'a>, ReadStorage<'a, Pos>, ReadStorage<'a, BBox>);

    fn run(&mut self, (es, pos, bbox): Self::SystemData) {
        //TODO: optimize for less than O^2(|entities|)
        for (id1, pos1, bbox1) in (&*es, &pos, &bbox).join() {
            for (id2, pos2, bbox2) in (&*es, &pos, &bbox).join() {
                if self.handlers.contains_key(&(bbox1.btype, bbox2.btype))
                    && id1 < id2
                    && bbox_collide(pos1, bbox1.size, pos2, bbox2.size) {
                    self.collisions.push(CollisionInfo(bbox1.btype, id1, bbox2.btype, id2));
                }
            }
        }
    }
}

pub type CollisionHandler = Fn(&mut World, Entity, Entity) -> ();

pub struct OutsideInfo(pub BType, pub Entity);

pub type OutsideHandler = Fn(&mut World, Entity) -> ();

pub struct OutsideSys {
    size: Size,
    pub outsiders: Vec<OutsideInfo>,
    handlers: BTreeMap<BType, Box<OutsideHandler>>
}

impl OutsideSys {
    pub fn new(size: Size) -> OutsideSys {
        OutsideSys {
            size: size,
            outsiders: Vec::new(),
            handlers: BTreeMap::new()
        }
    }

    pub fn add_handler(&mut self, btype: BType, handler: Box<OutsideHandler>) {
        self.handlers.insert(btype, handler);
    }

    pub fn process(&mut self, world: &mut World) {
        for info in &self.outsiders {
            let &OutsideInfo(t1, e1) = info;
            match self.handlers.get(&t1) {
                Some(handler) => handler(world, e1),
                None => panic!("Unknown outside behaviour for {}", t1)
            }
        }
        self.outsiders.clear();
    }
}

//CONCERN: epsilon?
fn bbox_outside(pos: &Pos, bbox: Size, region: Size) -> bool {
    (pos.x + bbox.w < 0. || pos.x > region.w ||
     pos.y + bbox.h < 0. || pos.y > region.h)
}

impl<'a> System<'a> for OutsideSys {
    type SystemData = (Entities<'a>, ReadStorage<'a, Pos>, ReadStorage<'a, BBox>);

    fn run(&mut self, (es, pos, bbox): Self::SystemData) {
        self.outsiders.clear();
        for (id, pos, bbox) in (&*es, &pos, &bbox).join() {
            if self.handlers.contains_key(&bbox.btype)
                && bbox_outside(pos, bbox.size, self.size) {
                self.outsiders.push(OutsideInfo(bbox.btype, id));
            }
        }
    }
}
*/
