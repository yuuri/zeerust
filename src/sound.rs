use ggez::audio;

pub type Sound = audio::Source;
