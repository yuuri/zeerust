use ggez::graphics::{Point2, Vector2};
pub use ggez::graphics::Rect;

#[derive(Clone, Copy, Default)]
pub struct Size {
    pub x: f32,
    pub y: f32
}

pub type Point = Point2;
pub type Vector = Vector2;