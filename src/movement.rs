use specs::{Component, System, VecStorage, DenseVecStorage, HashMapStorage, ReadStorage, WriteStorage, Join};

pub struct Pos {
    pub x: f32,
    pub y: f32
}

impl Component for Pos {
    type Storage = VecStorage<Self>;
}

//#[derive(Clone, Copy)]
pub struct Vel {
    pub x: f32,
    pub y: f32
}

impl Vel {
    pub fn from_dir(dir: f32, vel: f32) -> Vel {
        Vel {
            x: dir.cos() * vel,
            y: dir.sin() * vel
        }
    }
}

impl Default for Vel {
    fn default() -> Self {
        Vel {x: 0., y: 0.}
    }
}

impl Component for Vel {
    type Storage = DenseVecStorage<Self>;
}

pub struct MoveSys;

impl<'a> System<'a> for MoveSys {
    type SystemData = (ReadStorage<'a, Vel>, WriteStorage<'a, Pos>);

    fn run(&mut self, (vels, mut poss): Self::SystemData) {
        for (vel, pos) in (&vels, &mut poss).join() {
            pos.x += vel.x;
            pos.y += vel.y
        }
    }
}

pub struct Acc {
    pub x: f32,
    pub y: f32
}

impl Component for Acc {
    type Storage = HashMapStorage<Self>;
}

pub struct AccSys;

impl<'a> System<'a> for AccSys {
    type SystemData = (ReadStorage<'a, Acc>, WriteStorage<'a, Vel>);

    fn run(&mut self, (accs, mut vels): Self::SystemData) {
        for (acc, vel) in (&accs, &mut vels).join() {
            vel.x += acc.x;
            vel.y += acc.y
        }
    }
}

pub struct Friction {
    pub x: f32,
    pub y: f32
}

impl Component for Friction {
    type Storage = HashMapStorage<Self>;
}

pub struct FrictionSys;

impl<'a> System<'a> for FrictionSys {
    type SystemData = (ReadStorage<'a, Vel>, WriteStorage<'a, Vel>);

    fn run(&mut self, (frictions, mut vels): Self::SystemData) {
        for (friction, vel) in (&frictions, &mut vels).join() {
            if vel.x.abs() < friction.x {
                vel.x = 0.
            } else {
                vel.x -= friction.x * vel.x.signum()
            }
            if vel.y.abs() < friction.y {
                vel.y = 0.
            } else {
                vel.y -= friction.y * vel.y.signum()
            }
        }
    }
}
