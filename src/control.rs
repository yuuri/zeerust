extern crate enum_map;

pub use ggez::event::{Keycode};
use std::collections::HashMap;

pub struct KbdState {
    pub keycode: Keycode
}

pub struct InputState<T> where T: enum_map::Enum<bool> {
    state: enum_map::EnumMap<T, bool>
}

impl<T> InputState<T> where T: enum_map::Enum<bool> {
    fn pressed(&self, key: T) -> bool {
        false
    }

    fn press(&mut self, key: T) -> bool {
        false
    }

    fn release(&mut self, key: T) -> bool {
        false
    }
}

pub struct InputMapper<T> {
    mapper: HashMap<Keycode, T>
}

impl<T> InputMapper<T> where T: Copy {
    fn new(mappings: &[(Keycode, T)]) -> InputMapper<T> {
        let mut mapper = HashMap::<Keycode, T>::new();
        for &(ref key, ref val) in mappings.iter() {
            mapper.insert(*key, *val);
        }
        InputMapper::<T> {
            mapper: mapper
        }
    }

    fn get(&self, key: Keycode) -> Option<T> {
        match self.mapper.get(&key) {
            Some(val) => Some(*val),
            None => None
        }
    }
}
