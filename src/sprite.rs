use common::Point;
use graphics::*;
use std::sync::Arc;
use specs::{Component, DenseVecStorage};

#[derive(Clone)]
pub struct Sprite {
    pub image: Arc<Image>,
    pub visible: bool,
    pub scale: Point
}

impl Sprite {
    pub fn new(image: Image) -> Sprite {
        Sprite {
            image: Arc::new(image),
            visible: true,
            scale: Point::new(1., 1.)
        }
    }

    pub fn width(&self) -> f32 {
        return self.image.width() as f32;
    }

    pub fn height(&self) -> f32 {
        return self.image.height() as f32;
    }
}

impl Component for Sprite {
    type Storage = DenseVecStorage<Self>;
}

impl Drawable for Sprite {
    fn draw_ex(&self, ctx: &mut Context, param: DrawParam) -> GameResult<()> {
        if self.visible {
            let mut param = param;
            param.scale = self.scale;
            self.image.draw_ex(ctx, param)
        } else {
            Ok(())
        }
    }

    fn set_blend_mode(&mut self, mode: Option<BlendMode>) {
    }

    fn get_blend_mode(&self) -> Option<BlendMode> {
        None
    }
}

/*pub struct Animated {
}*/
