use specs::{World};
use movement::{Pos, Vel, Acc};
use collision::{Size, BBox};
use sprite::Sprite;
use background::Background;

pub struct Room {
    pub size: Size,
    pub world: World
}

pub type ObjectAdder = Fn(&mut World) -> ();

impl Room {
    pub fn new(size: Size) -> Room {
        let mut world = World::new();
        world.add_resource(size);
        world.register::<Pos>();
        world.register::<Vel>();
        world.register::<Acc>();
        world.register::<Sprite>();
        world.register::<Background>();
        world.register::<BBox>();
        Room {
            size: size,
            world: world
        }
    }

    pub fn add(&mut self, adder: &ObjectAdder) {
        adder(&mut self.world);
    }
}
