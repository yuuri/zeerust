use ggez::{Context, GameResult};
use ggez::graphics::*;
use specs::{Component, HashMapStorage};
use std::ops::Deref;
use std::sync::Arc;

#[derive(Clone)]
pub struct Background {
    image: Arc<Image>,
}

impl Background {
    pub fn new(image: Image, repeat_x: bool, repeat_y: bool) -> Background {
        let mut image = image;
        //image.set_wrap(WrapMode::Tile, WrapMode::Tile);
        Background {
            image: Arc::new(image)
        }
    }
}

impl Deref for Background {
    type Target = Image;

    fn deref(&self) -> &Self::Target {
        &self.image
    }
}

impl Component for Background {
    type Storage = HashMapStorage<Self>;
}

impl Drawable for Background {
    fn draw_ex(&self, ctx: &mut Context, param: DrawParam) -> GameResult<()> {
        let size = get_screen_coordinates(ctx);

        for i in 0..(size.w as u32 / self.image.width() + 1) {
            for j in 0..(size.h.abs() as u32 / self.image.height() + 1) {
                let mut tmp = param.clone();
                tmp.dest.x += (i * self.image.width() + self.image.width() / 2) as f32;
                tmp.dest.y += (j * self.image.height() + self.image.height() / 2) as f32;
                self.image.draw_ex(ctx, tmp); //TODO: process errors
            }
        }

        Ok(())
    }

    fn set_blend_mode(&mut self, mode: Option<BlendMode>) {
    }

    fn get_blend_mode(&self) -> Option<BlendMode> {
        None
    }
}
