extern crate zeerust;
extern crate specs;

use zeerust::{Pos, Vel, MoveSys};
use specs::{World, DispatcherBuilder};

#[test]
fn movesys_test() {
    let mut world = World::new();
    world.register::<Pos>();
    world.register::<Vel>();

    let id = world.create_entity().with(Pos{x: 0., y: 0.}).with(Vel{x: 1.0, y: -1.0}).build();

    let mut dispatcher = DispatcherBuilder::new()
        .add(MoveSys, "move", &[])
        .build();

    dispatcher.dispatch(&mut world.res);
    let st = world.read::<Pos>();
    let new_pos = st.get(id).unwrap();

    assert_eq!(new_pos.x, 1.0);
    assert_eq!(new_pos.y, -1.0);
}

/*
fn bounce_against(spd1: &mut Spd, spd2: &mut Spd, dir: Direction) {
    match dir {
        Direction::Left | Direction::Right => {spd1.dx = -spd1.dx; spd2.dx = -spd2.dx}
        _ => {spd1.dy = -spd1.dy; spd2.dy = -spd2.dy}
}

impl<'a> System<'a> for BounceSys {
    type SystemData = (ReadStorage<'a, Spd>, WriteStorage<'a, Spd>);
}

#[test]
fn bounce_test() {
    let mut world = World::new();
    world.register::<Pos>();
    world.register::<Spd>();
    world.register::<BBox>();

    let bouncer = 1;

    let id1 = world.create_entity()
        .with(Pos{x: 0., y: 0.})
        .with(Spd{dx: 8., dy: 0.})
        .with(bbox(bouncer, 16., 16.))
        .build();

    let id2 = world.create_entity()
        .with(Pos{x: -8., y: 24.})
        .with(Spd{dx: -8., dy: 8.})
        .with(bbox(bouncer, 16., 16.))
        .build();

    let mut csys = ColliderSys::new();
    csys.register(bouncer, bouncer

    let mut dispatcher = DispatcherBuilder::new()
        .add(MoveSys, "move", &[])
        .add(, "collision", &["move"])
        .build();

    dispatcher.dispatch(&mut world.res);
    dispatcher.dispatch(&mut world.res);
    dispatcher.dispatch(&mut world.res);
}
*/
