extern crate zeerust;
extern crate specs;
extern crate ggez;

use zeerust::{Pos, Vel, Sprite, Room, GameBuilder};
use zeerust::collision::{BBox, Size, bbox_collide};
use zeerust::background::*;
use zeerust::control::*;
use zeerust::sound::*;

//TODO: get rid of library-specific imports
use specs::{Component, System, ReadStorage, WriteStorage, HashMapStorage, NullStorage, Fetch, Join};
use ggez::{Context, conf, event, graphics};

struct Pad {
    player_id: bool
}

impl Pad {
    fn up_key(&self) -> Keycode {
        if self.player_id {Keycode::W} else {Keycode::Up}
    }

    fn down_key(&self) -> Keycode {
        if self.player_id {Keycode::S} else {Keycode::Down}
    }
}

impl Component for Pad {
    type Storage = HashMapStorage<Self>;
}

struct PadKeyDown;

impl<'a> System<'a> for PadKeyDown {
    type SystemData = (Fetch<'a, KbdState>, ReadStorage<'a, Pad>, WriteStorage<'a, Vel>);

    fn run (&mut self, (kbd, pads, mut vels): Self::SystemData) {
        let move_vel = 4.0;

        for (pad, mut vel) in (&pads, &mut vels).join() {
            if kbd.keycode == pad.up_key() {
                vel.y = -move_vel
            } else if kbd.keycode == pad.down_key() {
                vel.y = move_vel
            }
        }
    }
}

struct PadKeyUp;

impl<'a> System<'a> for PadKeyUp {
    type SystemData = (Fetch<'a, KbdState>, ReadStorage<'a, Pad>, WriteStorage<'a, Vel>);

    fn run (&mut self, (kbd, pads, mut vels): Self::SystemData) {
        for (pad, mut vel) in (&pads, &mut vels).join() {
            if kbd.keycode == pad.up_key() || kbd.keycode == pad.down_key() {
                vel.y = 0.
            }
        }
    }
}

#[derive(Default)]
struct Ball;

impl Component for Ball {
    type Storage = NullStorage<Self>;
}

struct Sounds {
    bounce: Sound
}

impl Sounds {
    fn new(ctx: &mut Context) -> Sounds {
        Sounds {
            bounce: Sound::new(ctx, "/pong.ogg").unwrap()
        }
    }
}

struct BounceBall;

impl<'a> System<'a> for BounceBall {
    type SystemData = (Fetch<'a, Sounds>, ReadStorage<'a, Pad>, ReadStorage<'a, Ball>,
                       ReadStorage<'a, Pos>, ReadStorage<'a, BBox>, WriteStorage<'a, Vel>);

    fn run (&mut self, (sounds, pads, balls, poss, bboxs, mut vels): Self::SystemData) {
        for (_, pad_pos, pad_bbox) in (&pads, &poss, &bboxs).join() {
            for (_, ball_pos, ball_bbox, mut ball_vel) in (&balls, &poss, &bboxs, &mut vels).join() {
                if bbox_collide(pad_pos, pad_bbox.size, ball_pos, ball_bbox.size) {
                    sounds.bounce.play();
                    ball_vel.y = (ball_pos.y - pad_pos.y) / 12.0;
                    ball_vel.x = -ball_vel.x;
                }
            }
        }
    }
}

struct BallOutside;

impl<'a> System<'a> for BallOutside {
    type SystemData = (Fetch<'a, Size>, ReadStorage<'a, Ball>, WriteStorage<'a, Pos>, WriteStorage<'a, Vel>);

    fn run (&mut self, (size, balls, mut poss, mut vels): Self::SystemData) {
        for (_, mut pos, mut vel) in (&balls, &mut poss, &mut vels).join() {
            if pos.y < 0. || pos.y > size.x {
                vel.y = -vel.y;
            }

            if pos.x < 0. || pos.x > size.x {
                pos.x = size.x / 2.;
                pos.y = size.y / 2.;
            }
        }
    }
}

fn main() {
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("pong", "zeerust", c).unwrap();

    let mut game = {
        let (width, height) = (800., 600.);
        let mut room = Room::new(Size{x: width, y: height});
        {
            //Background init
            room.world.register::<Background>();

            let back_image = graphics::Image::new(ctx, "/dirt.png").unwrap();
            room.world.create_entity()
                .with(Background::new(back_image, true, true))
                .with(Pos{x: 0., y: 0.})
                .build();
        }
        {
            //Pad init
            room.world.register::<Pad>();

            let pad_bbox = BBox::new(16., 48., true);
            let ppad_sprite = Sprite::new(graphics::Image::new(ctx, "/pad.png").unwrap());
            let mut create_pad = |id: bool, pos: Pos| {
                room.world.create_entity()
                    .with(Pad {player_id: id}).with(pos).with(pad_bbox.clone())
                    .with(Vel::default()).with(ppad_sprite.clone())
                    .build();
            };
            create_pad(true, Pos{x: 16., y: height / 2.});
            create_pad(false, Pos{x: width - 16., y:height / 2.})
        }
        {
            //Ball init
            let ball_sprite = Sprite::new(graphics::Image::new(ctx, "/ball.png").unwrap());

            room.world.register::<Ball>();
            room.world.create_entity()
                .with(Ball).with(Pos{x: width / 2., y: height / 2.})
                .with(BBox::new(24., 24., false)).with(Vel{x: 4., y: 0.}).with(ball_sprite)
                .build();
        }
        {
            //Sounds init
            room.world.add_resource(Sounds::new(ctx))
        }

        let mut gb = GameBuilder::new(room);

        gb.on_step = gb.on_step
            .add(BounceBall, "bounce_ball", &["move"])
            .add(BallOutside, "ball_outside", &["move"]);

        gb.on_key_down = gb.on_key_down
            .add(PadKeyDown, "pad_key_down", &[]);

        gb.on_key_up = gb.on_key_up
            .add(PadKeyUp, "pad_key_up", &[]);

        gb.build()
    };

    if let Err(e) = event::run(ctx, &mut game) {
        println!("Error encountered: {}", e);
    } else {
        println!("Demo exited cleanly.");
    }

}
