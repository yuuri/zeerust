extern crate zeerust;
extern crate ggez;

use zeerust::tiles::*;
use ggez::{Context, GameResult, conf, event};
use ggez::graphics::*;

use std::sync::Arc;

type TestSet = GenericTileset<()>;

struct TileDemo {
    layer: TileLayer<TestSet>
}

impl event::EventHandler for TileDemo {
    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        clear(ctx);
        self.layer.draw_ex(ctx, DrawParam::default());
        present(ctx);
        Ok(())
    }

    fn update(&mut self, _ctx: &mut ggez::Context, _duration: std::time::Duration) -> GameResult<()> {
        Ok(())
    }
}

fn main() {
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("tiles", "zeerust", c).unwrap();

    let tiles_image = Image::new(ctx, "/tileset.png").unwrap();
    let ts = TestSet::new(tiles_image, Size {x: 64., y: 64.});
    let mut layer = TileLayer::<TestSet>::new(Arc::new(ts));
    let tile_coords: [(Cell, Cell); 9] = [
        ((1,2), (1,0)),
        ((2,2), (2,0)),
        ((3,2), (3,0)),
        ((5,2), (0,2)),

        ((1,3), (1,1)),
        ((2,3), (2,1)),
        ((3,3), (1,0)),
        ((4,3), (2,0)),
        ((5,3), (3,0)),
    ];

    for &(cell, id) in &tile_coords {
        layer.add(cell, id);
    }

    let mut demo = TileDemo {
        layer: layer
    };

    if let Err(e) = event::run(ctx, &mut demo) {
        println!("Error encountered: {}", e);
    } else {
        println!("Demo exited cleanly.");
    }
}
