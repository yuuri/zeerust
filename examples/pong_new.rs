extern crate zeerust;
extern crate specs;
extern crate ggez;

use zeerust::{Pos, Spd, Vis, Sprite, Room, GameBuilder};
use zeerust::collision::{BBox, Size, bbox_collide};
use zeerust::control::{Keycode, KbdState};

//TODO: get rid of library-specific imports
use specs::{Component, System, ReadStorage, WriteStorage, HashMapStorage, NullStorage, Fetch, Join};
use ggez::{Context, conf, event, graphics};

struct Pad {
    player_id: bool
}

impl Pad {
    fn up_key(&self) -> Keycode {
        if self.player_id {Keycode::W} else {Keycode::Up}
    }

    fn down_key(&self) -> Keycode {
        if self.player_id {Keycode::S} else {Keycode::Down}
    }
}

/*
key_handler!({
})
*/

impl OnKeyDown for Pad {
    type Data = &mut Spd;

    fn on_key_down(&mut self, kbd: KbdState, data: Data) {
        const pad_spd: f32 = 4.;
        if kbd.keycode == self.up_key() {
            spd.dy = -pad_spd
        } else if kbd.keycode == self.down_key() {
            spd.dy = pad_spd
        }
    }

impl OnKeyUp for Pad {
    type Data = &mut Spd;

    fn on_key_up(&mut self, kbd: KbdState, data: Data) {
        if kbd.keycode == pad.up_key() || kbd.keycode == pad.down_key() {
            spd.dy = 0.
        }
    }
}

impl GameObject for Pad {
    type Storage = HashMapStorage<Self>;
    type Components = (Vis, Pos);

    fn register(gb: &mut GameBuilder) {
        gb.on_step = gb.on_step
            .add(BounceBall, "bounce_ball", &["move"])
            .add(BallOutside, "ball_outside", &["move"]);

        gb.on_key_down = gb.on_key_down
            .add(KeyDown<Pad>, "pad_key_down", &[]);

        gb.on_key_up = gb.on_key_up
            .add(KeyUp<Pad>, "pad_key_up", &[]);
    }

    /*
    fn create(room: &mut Room, data: Self::Components) -> Entity {
        room.world.create_entity()
            .with(Pad {player_id: id}).with(pos).with(pad_bbox.clone())
            .with(Spd::default()).with(Vis::new(&pad_sprite))
            .build();
    }
    */
}

/*
impl Component for Pad {
    type Storage = HashMapStorage<Self>;
}

struct PadKeyDown;

impl<'a> System<'a> for PadKeyDown {
    type SystemData = (Fetch<'a, KbdState>, ReadStorage<'a, Pad>, WriteStorage<'a, Spd>);

    fn run (&mut self, (kbd, pads, mut spds): Self::SystemData) {
        for (pad, mut spd) in (&pads, &mut spds).join() {
            if kbd.keycode == pad.up_key() {
                spd.dy = -pad_spd
            } else if kbd.keycode == pad.down_key() {
                spd.dy = pad_spd
            }
        }
    }
}

struct PadKeyUp;

impl<'a> System<'a> for PadKeyUp {
    type SystemData = (Fetch<'a, KbdState>, ReadStorage<'a, Pad>, WriteStorage<'a, Spd>);

    fn run (&mut self, (kbd, pads, mut spds): Self::SystemData) {
        for (pad, mut spd) in (&pads, &mut spds).join() {
            if kbd.keycode == pad.up_key() || kbd.keycode == pad.down_key() {
                spd.dy = 0.
            }
        }
    }
}
*/

struct Scoreboard {
    score_left: u32,
    score_right: u32
}

impl OnDraw for Scoreboard {
    type Data = &Pos;

    fn on_draw(&self, ctx: &mut Context, pos: Self::Data) {
        let score = self.score_left.to_string() + ":" + self.score_right.to_string();
        let text = Text::new(&score);
        text.draw(ctx, Point {x: pos.x, y: pos.y})
    }
}

impl GameObject for Scoreboard {
    type Storage = HashMapStorage<Self>;
    type Components = Pos;

    fn register(gb: &mut GameBuilder) {
        gb.on_draw = gb.on_draw
            .add(Draw<Self>, "draw_scoreboard", &[])
    }
}

#[derive(Default)]
struct Ball;

impl GameObject for Ball {
    type Storage = NullStorage<Self>;
}

impl OnStep for Ball {
    type Data = (&mut Scoreboard, &mut Pos, &mut Spd);

    fn on_step(&mut self, (&mut board, &mut pos, &mut spd: Self::Data) {
        if pos.y < 0. || pos.y > size.h {
            spd.dy = -spd.dy
        }

        if pos.x < 0. || pos.x > size.w {
            if pos.x < 0. {
                board.score_left += 1
            } else {
                board.score_right += 1
            }
            pos.x = size.w / 2.;
            pos.y = size.w / 2.
        }
    }
}

/*
impl Component for Ball {
    type Storage = NullStorage<Self>;
}

struct BounceBall;*/

/*trait Collision<A, B> {
}

impl<'a, A, B> System<'a> for Collision<A, B> {
    type SystemTarget = None;
}*/

/*impl BounceBall {
    fn collide(pad_pos: &Pos, ball_pos: &Pos, ball_spd: &mut Spd) {
        ball_spd.dx = (ball_pos.y - pad_pos.y) / 12.0;
        ball_spd.dy = -ball_spd.dy;
    }
}*/

/*
impl<'a> System<'a> for BounceBall {
    type SystemData = (ReadStorage<'a, Pad>, ReadStorage<'a, Ball>, ReadStorage<'a, Pos>, ReadStorage<'a, BBox>, WriteStorage<'a, Spd>);

    fn run (&mut self, (pads, balls, poss, bboxs, mut spds): Self::SystemData) {
        for (_, pad_pos, pad_bbox) in (&pads, &poss, &bboxs).join() {
            for (_, ball_pos, ball_bbox, mut ball_spd) in (&balls, &poss, &bboxs, &mut spds).join() {
                if bbox_collide(pad_pos, pad_bbox.size, ball_pos, ball_bbox.size) {
                    ball_spd.dy = (ball_pos.y - pad_pos.y) / 12.0;
                    ball_spd.dx = -ball_spd.dx;
                }
            }
        }
    }
}

struct BallOutside;

impl<'a> System<'a> for BallOutside {
    type SystemData = (Fetch<'a, Size>, ReadStorage<'a, Ball>, WriteStorage<'a, Pos>, WriteStorage<'a, Spd>);

    fn run (&mut self, (size, balls, mut poss, mut spds): Self::SystemData) {
        for (_, mut pos, mut spd) in (&balls, &mut poss, &mut spds).join() {
            if pos.y < 0. || pos.y > size.h {
                spd.dy = -spd.dy;
            }

            if pos.x < 0. || pos.x > size.w {
                pos.x = size.w / 2.;
                pos.y = size.w / 2.;
            }
        }
    }
}
*/

fn main() {
    let conf = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("pong", "zeerust", conf).unwrap();

    let mut game = {
        let (width, height) = (conf.window_height as f32, conf.window_width as f32);
        let mut room = Room::new(width, height);
        {
            //Pad init
            room.register::<Pad>();

            room.create::<Pad>(Pad {player_id: true}, Pos {x: 16., y: height / 2.});
            room.create::<Pad>(Pad {player_id: false}, Pos {x: width - 16., y: height / 2.});
            /*
            room.world.register::<Pad>();

            let pad_bbox = BBox::new(16., 48., true);
            let pad_sprite = Sprite::new(graphics::Image::new(ctx, "/pad.png").unwrap());
            let mut create_pad = |id: bool, pos: Pos| {
                room.world.create_entity()
                    .with(Pad {player_id: id}).with(pos).with(pad_bbox.clone())
                    .with(Spd::default()).with(Vis::new(&pad_sprite))
                    .build();
            };
            create_pad(true, Pos{x: 16., y: height / 2.});
            create_pad(false, Pos{x: width - 16., y:height / 2.})
            */
        }
        {
            //Ball init
            room.register::<Ball>();

            room.create::<Ball>(Pos{x: width / 2, y: height / 2});
            /*
            let ball_sprite = Sprite::new(graphics::Image::new(ctx, "/ball.png").unwrap());

            room.world.register::<Ball>();
            room.world.create_entity()
                .with(Ball).with(Pos{x: width / 2., y: height / 2.})
                .with(BBox::new(24., 24., false)).with(Spd{dx: 4., dy: 0.}).with(Vis::new(&ball_sprite))
                .build();
            */
        }

        /*let mut gb = GameBuilder::new(room);

        gb.on_step = gb.on_step
            .add(BounceBall, "bounce_ball", &["move"])
            .add(BallOutside, "ball_outside", &["move"]);

        gb.on_key_down = gb.on_key_down
            .add(PadKeyDown, "pad_key_down", &[]);

        gb.on_key_up = gb.on_key_up
            .add(PadKeyUp, "pad_key_up", &[]);

        gb.build()*/
    };

    if let Err(e) = event::run(ctx, &mut game) {
        println!("Error encountered: {}", e);
    } else {
        println!("Demo exited cleanly.");
    }

}
