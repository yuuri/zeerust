extern crate zeerust;
extern crate ggez;
extern crate specs;

use specs::{Entity, Component, System, World, ReadStorage, WriteStorage, NullStorage, Fetch, Join};
use zeerust::{Game, Room, Sprite, Size, Pos, Vel, BBox, KbdState};
use ggez::{Context, conf, event, graphics};
use ggez::event::Keycode;

#[derive(Default)]
struct Actor;

impl Component for Actor {
    type Storage = NullStorage<Self>;
}

struct ActorSys;

impl<'a> System<'a> for ActorSys {
    type SystemData = (Fetch<'a, KbdState>, ReadStorage<'a, Actor>, WriteStorage<'a, Vel>);

    fn run(&mut self, (state, actor, mut vel): Self::SystemData) {
        for (_, vel) in (&actor, &mut vel).join() {
            match state.keycode {
                Keycode::Right => vel.x = 1.0,
                Keycode::Left  => vel.x = -1.0,
                Keycode::Up    => vel.y = -1.0,
                Keycode::Down  => vel.y = 1.0,
                _ => ()
            }
        }
    }
}

pub fn main() {
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("actor", "zeerust", c).unwrap();

    let spr = Sprite::new(graphics::Image::new(ctx, "/block.png").unwrap());

    /*let square = |pos: Pos, vel: Vel| {
        |world: &mut World| {world.create_entity().with(pos).with(vel).with(Vis::new(Arc::clone(&image1))).build();}
    };*/

    let width = 800.;
    let height = 600.;
    let mut room = Room::new(Size{x: width, y: height});

    room.world.create_entity()
        .with(Pos{x: width / 2.0, y: height / 2.0}).with(Vel::default())
        .with(spr)
        .with(Actor)
        .build();

    let mut gb = GameBuilder::new(room);
    gb.on_key_down = gb.on_key_down
        .add(ActorSys, "actor", &[]);

    let game = &mut Game::new(room, kbd);

    if let Err(e) = event::run(ctx, game) {
        println!("Error encountered: {}", e);
    } else {
        println!("Demo exited cleanly.");
    }
}
