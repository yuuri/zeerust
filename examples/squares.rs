extern crate zeerust;
extern crate specs;
extern crate ggez;
extern crate rand;

use std::collections::HashSet;
use rand::Rng;

use zeerust::{Entity, Room, GameBuilder, Sprite, Pos, Vel};
use zeerust::collision::{BBox, Size, Rect, bbox_collide};

//TODO: get rid of dependencies imports
use specs::{Entities, Builder, Component, System, Read, NullStorage, ReadStorage, WriteStorage, Join};
use ggez::{Context, conf, event, graphics};

#[derive(Default)]
struct Block;

impl Component for Block {
    type Storage = NullStorage<Self>;
}

struct BlockBounce;

impl<'a> System<'a> for BlockBounce {
    type SystemData = (Entities<'a>, ReadStorage<'a, Block>, ReadStorage<'a, Pos>,
                       ReadStorage<'a, BBox>, WriteStorage<'a, Vel>);

    fn run(&mut self, (es, blocks, poss, bboxs, mut vels): Self::SystemData) {
        //TODO: optimize for less than O^2(|entities|)
        let mut collisions = Vec::<(Entity, Entity)>::new();
        for (id1, _, pos1, bbox1) in (&*es, &blocks, &poss, &bboxs).join() {
            for (id2, _, pos2, bbox2) in (&*es, &blocks, &poss, &bboxs).join() {
                if id1 < id2 {
                    if let Some(dir) = bbox_collide(&pos1, &bbox1, &pos2, &bbox2) {
                        collisions.push((id1, id2));
                    }
                }
            }
        }

        for (id1, id2) in collisions {
            let pos1 = poss.get(id1).unwrap();
            let pos2 = poss.get(id2).unwrap();
            let dx = (pos1.x - pos2.x).abs();
            let dy = (pos1.y - pos2.y).abs();
            {
                let mut vel1 = vels.get_mut(id1).unwrap();
                if dx >= dy { vel1.x = -vel1.x; }
                if dy >= dx { vel1.y = -vel1.y; }
            }
            {
                let mut vel2 = vels.get_mut(id2).unwrap();
                if dx >= dy { vel2.x = -vel2.x; }
                if dy >= dx { vel2.y = -vel2.y; }
            }
        }
    }
}

struct BlockOutside;

impl<'a> System<'a> for BlockOutside {
    type SystemData = (Read<'a, Size>, ReadStorage<'a, Block>, ReadStorage<'a, Pos>,
                       ReadStorage<'a, BBox>, WriteStorage<'a, Vel>);

    fn run(&mut self, (size, blocks, poss, bboxs, mut vels): Self::SystemData) {
        for (_, pos, bbox, mut vel) in (&blocks, &poss, &bboxs, &mut vels).join() {
            if pos.x + bbox.rect.x < 0. || pos.x + bbox.rect.x + bbox.rect.w > size.x {
                vel.x = -vel.x
            }
            if pos.y + bbox.rect.x < 0. || pos.y + bbox.rect.y + bbox.rect.h > size.y {
                vel.y = -vel.y
            }
        }
    }
}

pub fn main() {
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("squares", "zeerust", c).unwrap();

    let spr = Sprite::new(graphics::Image::new(ctx, "/block.png").unwrap());
    let block_size = spr.width();
    let half_size = spr.width() / 2.;
    let bbox = BBox{rect: Rect::new(-half_size, -half_size, block_size, block_size), solid: true};

    let (width, height) = (800., 600.);
    let mut room = Room::new(Size {x: width, y: height});
    room.world.register::<Block>();

    let mut rng = rand::thread_rng();
    let mut taken = HashSet::<(u32, u32)>::new();
    for _i in 0 .. 50 {
        let x = rng.gen_range::<u32>(1, (width / spr.width()) as u32);
        let y = rng.gen_range::<u32>(1, (height / spr.height()) as u32);
        if !taken.contains(&(x, y)) {
            taken.insert((x, y));
            let pos = Pos {
                x: x as f32 * block_size,
                y: y as f32 * block_size
            };
            let vel = Vel::from_dir(
                rng.gen_range::<f32>(0., 2. * std::f32::consts::PI),
                2.
            );
            room.world.create_entity()
                .with(Block).with(pos).with(vel)
                .with(bbox.clone()).with(spr.clone()).build();
        }
    }

    let mut gb = GameBuilder::new(room);

    gb.on_step = gb.on_step
        .with(BlockBounce, "block_bounce", &["move"])
        .with(BlockOutside, "block_outside", &["move"]);

    let mut game = gb.build();
    if let Err(e) = event::run(ctx, &mut game) {
        println!("Error encountered: {}", e);
    } else {
        println!("Demo exited cleanly.");
    }
}
